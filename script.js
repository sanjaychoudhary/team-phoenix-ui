document.addEventListener("DOMContentLoaded", function () {
    const urlForm = document.getElementById("urlForm");

    urlForm.addEventListener("submit", function (e) {
        e.preventDefault();
        const urlInput = document.getElementById("url");
        const url = urlInput.value;

        // testing
        window.location.href = `result.html?url=${encodeURIComponent(url)}`;
    });
});
