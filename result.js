document.addEventListener("DOMContentLoaded", function () {
    // Get the URL from the query parameter (for future model)
    const urlParams = new URLSearchParams(window.location.search);
    const url = urlParams.get("url");

    // list of whitelisted and blacklisted URLs for testing our UI.
    const whitelist = [
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.amazon.com",
        "https://www.netflix.com",
        "https://www.apple.com",
        "https://www.microsoft.com",
        "https://www.twitter.com",
        "https://www.instagram.com",
        "https://www.linkedin.com",
        "https://www.youtube.com"
    ];
    
    const blacklist = [
        "https://www.g00gle.com",
        "https://www.faceb00k.com",
        "https://www.amaz0n.com",
        "https://www.netflixx.com",
        "https://www.appl3.com",
        "https://www.micr0soft.com",
        "https://www.twitt3r.com",
        "https://www.instagr4m.com",
        "https://www.l1nkedin.com",
        "https://www.y0utube.com"
    ];

    const resultDiv = document.getElementById("result");

    if (whitelist.includes(url)) {
        resultDiv.innerHTML = `
            <img src="https://www.svgrepo.com/show/423679/store-verified-shopping.svg" alt="Safe Icon" class="icon">
            <p><b>${url} </b> is safe.</p>
            <p>✅ You are good to go ahead. 🔒</p>
        `;
    } else if (blacklist.includes(url)) {
        resultDiv.innerHTML = `
            <img src="https://www.svgrepo.com/show/507073/con-warning.svg" alt="Warning Icon" class="icon">
            <p><b>${url}</b> is detected as <b> Malicious. ☠️</p>
            <p> <b>🆘 Not safe to visit. ❌</p>
        `;
    } else {
        resultDiv.innerHTML = `
            <img src="https://www.svgrepo.com/show/521253/unlinked-1.svg" alt="Warning Icon" class="icon">
            <p>${url} is not in our whitelist. Proceed with caution. ♻️</p>
        `;
    }
});
